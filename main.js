const addBtn = document.querySelectorAll('[data-notes-add]');
const deleteBtn = document.querySelectorAll('[data-notes-delete]')
const saveBtn = document.querySelectorAll('[data-notes-save]');
const menuList = document.querySelector('[data-notes-menu-list]');

const noteEditor = document.querySelector('[data-notes-editor]');
const notePlaceholder = document.querySelector('[data-notes-placeholder]');
const titleInput = document.querySelector('[data-notes-title]');
const noteArea = document.querySelector('[data-notes-area]');
const dateField = document.querySelector('[data-notes-date]');
const savedModal = document.querySelector('[data-saved-modal]');

// Initialize empty view
loadNoteIntoView();

const notes = loadLS() ?? {};
const listElements = {};
let currentNote = undefined;
let saved = true;

// Run after loading localstorage
loadDomObject();

addBtn.forEach(e => e.addEventListener('click', addNote));
deleteBtn.forEach(e => e.addEventListener('click', removeNote));
saveBtn.forEach(e => e.addEventListener('click', saveLS));

setInterval(() => {
    if (!saved)
        saveLS();
}, 10000);  // Save every 10 seconds if not saved


titleInput.addEventListener('input', e => {
    if (currentNote != null) {
        currentNote.title = e.target.value;

        refreshView();
    } else {
        addNote();
    }

    setSaved(false);
});

// Bring cursor to end of title
titleInput.addEventListener('click', e => {
    const len = e.target.value.length
    titleInput.focus();
    titleInput.setSelectionRange(len, len)
});

// Bring view of title to start
titleInput.addEventListener('blur', e => {
    titleInput.scrollTo(0,0);
}, true);

noteArea.addEventListener('input', e => {
    if (currentNote != null) {
        currentNote.content = e.target.value;

        setSaved(false);
    }
})

function loadDomObject() {
    Object.entries(notes).forEach(([id, e]) => {
        createElement(id);
    });

    refreshView();
}

function addNote() {
    const time = Date.now();

    notes[time] = {
        creationTimestamp: time,
        title: '',
        content: '',
    }

    const el = createElement(time);

    setSelection({ target: el });
    refreshView();

    setSaved(false);
}

function removeNote() {
    const id = currentNote.creationTimestamp;
    currentNote = undefined

    removeElement(id);
    delete notes[id];

    loadNoteIntoView();

    setSaved(false);
}

function removeElement(id) {
    menuList.removeChild(listElements[id]);

    listElements[id].remove();
    delete listElements[id];

    refreshView();
}

function createElement(id) {
    // Create the dom element
    const element = document.createElement('li');
    element.dataset.noteId = id;
    element.dataset.noteSelection = '';

    element.addEventListener('click', setSelection)

    listElements[id] = element;
    return element;
}

function refreshView() {
    Object.entries(listElements).forEach(([id, e]) => {
        if (!menuList.contains(e))
            menuList.appendChild(e);

        e.innerText = notes[id].title || formatTimestamp(notes[id].creationTimestamp);
    });

    if (currentNote === undefined)
        hideDelBtn();
    else
        showDelBtn();
}

function setSelection({ target }) {
    Object.entries(listElements).forEach(([id, e]) => {
        e.classList.remove('selected');
    });
    target.classList.add('selected');

    currentNote = notes[target.dataset.noteId];

    loadNoteIntoView(currentNote);

    noteArea.focus();
}

function loadNoteIntoView(note) {
    if (note === undefined) {
        titleInput.value = '';
        noteArea.value = '';
        dateField.innerText = '';

        hideDelBtn();
        hideNoteEditor();
        return
    }

    titleInput.value = note.title;
    noteArea.value = note.content;
    dateField.innerText = formatTimestamp(note.creationTimestamp, 'datetime');

    showNoteEditor();

    showDelBtn();
}

function formatTimestamp(ts, mode) {
    mode = mode ?? 'date';

    const date = new Date(ts);
    if (mode === 'date') {
        return date.toLocaleDateString();
    } else if (mode === 'datetime') {
        return date.toLocaleString();
    }

}

function hideDelBtn() {
    deleteBtn.forEach(e => e.style.display = 'none');
}

function showDelBtn() {
    deleteBtn.forEach(e => e.style.display = '');
}

function showNoteEditor() {
    noteEditor.classList.remove('hidden');
    notePlaceholder.classList.add('hidden');
}

function hideNoteEditor() {
    noteEditor.classList.add('hidden');
    notePlaceholder.classList.remove('hidden');
}

function setSaved(value) {
    saved = value;
    if (saved)
        savedModal.classList.add('hidden');
    else
        savedModal.classList.remove('hidden');
}

function loadLS() {
    return JSON.parse(window.localStorage.getItem('notesStore'));
}

function saveLS() {
    window.localStorage.setItem('notesStore', JSON.stringify(notes));
    setSaved(true);
}